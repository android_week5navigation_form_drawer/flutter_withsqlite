import 'dog.dart';
import 'dog_dao.dart';
import 'cat.dart';
import 'cat_dao.dart';

void main() async {
  var fido = Dog(id: 0, name: 'Fido', age: 35);
  var dido = Dog(id: 1, name: 'Dido', age: 20);

  await DogDao.insertDog(fido);
  await DogDao.insertDog(dido);
  print(await DogDao.dogs());

  fido = Dog(id: fido.id, name: fido.name, age: fido.age + 7);
  await DogDao.updateDog(fido);
  print(await DogDao.dogs());

  await DogDao.deleteDog(0);
  print(await DogDao.dogs());

// Cat Class

  var kiki = Cat(id: 0, name: 'kiki', age: 8);
  var lala = Cat(id: 1, name: 'lala', age: 9);

  await CatDao.insertCat(kiki);
  await CatDao.insertCat(lala);
  print(await CatDao.cats());

  kiki = Cat(id: kiki.id, name: kiki.name, age: kiki.age + 5);
  await CatDao.updateDog(kiki);
  print(await CatDao.cats());

  await CatDao.deleteCat(1);
  print(await CatDao.cats());
}
