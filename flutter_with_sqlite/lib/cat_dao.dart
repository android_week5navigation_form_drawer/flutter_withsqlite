import 'package:sqflite/sqflite.dart';
import 'databasepet.dart';
import 'cat.dart';

class CatDao {
  static Future<void> insertCat(Cat cat) async {
    final db = await CatDatabaseProvider.database;
    await db.insert(
      'cats',
      cat.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static Future<List<Cat>> cats() async {
    final db = await CatDatabaseProvider.database;

    return Cat.toList(await db.query('cats'));
  }

  static Future<void> updateDog(Cat cat) async {
    final db = await CatDatabaseProvider.database;
    await db.update(
      'cats',
      cat.toMap(),
      where: 'id = ?',
      whereArgs: [cat.id],
    );
  }

  static Future<void> deleteCat(int id) async {
    final db = await CatDatabaseProvider.database;
    await db.delete(
      'cats',
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
